
function defaults(obj,defaultProps)
{
    if(typeof obj == 'object' && typeof defaultProps == 'object')
    {
     let new_obj = {};

     for(let key in defaultProps)
       {
        if(obj[key] == undefined)
        {
            new_obj[key] = defaultProps[key];
          }
        else{
            new_obj[key] = obj[key]; //it will create that key automatic in new_obj andn put value against it 
           }
       }
    }
    else{
        return [];
    }        
}                                                 
module.exports = defaults
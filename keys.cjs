
function keys(obj)
{
    if(typeof obj == 'object')
    {
     let arr = [];
     for(const key in obj)
      {
        arr.push(key);   // Retrieve all the names of the object's properties.
      }
      return arr;
    }
    else{
        return [];
    }                          // Return the keys as strings in an array.                                       // Based on http://underscorejs.org/#keys
}
module.exports = keys
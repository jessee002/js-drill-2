function mapObject(obj,cb)
{
    if(typeof obj == 'object' && typeof cb == 'function')
    {
        let output = {};
        for(const key in obj)
        {
          output[key] = cb(obj[key]);
        }
        return output;
    }
    else{
        return [];
    }
}
/*if(!obj || !cb){   //if obj == null || cb(function == null) then it returns []
    return []
}*/

module.exports = mapObject;
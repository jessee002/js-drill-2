function invert(obj)
{
    if(typeof obj == 'object')
    {
     let new_obj = {};
     for(const key in obj)
     {
        new_obj[obj[key]] = key;   
      }
      return new_obj;
    }
    else{
        return [];
    }                                                    
}
module.exports = invert;
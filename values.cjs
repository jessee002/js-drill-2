function values(obj)
{
    if(typeof obj == 'object')
    {
        let arr = [];
        for(const key in obj)
        {
            arr.push(obj[key]);
        }
      return arr;
    }
    else{
        return [];
    }
}
module.exports = values;
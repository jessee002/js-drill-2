function pairs(obj)
{
    if(typeof obj == 'object')
    {
        let arr = [];
        for(const key in obj)
        {
            arr.push([key,obj[key]]);
        }
        return arr;
    }else{
        return [];
    }
}
module.exports = pairs